#!/bin/bash

export SWWW_TRANSITION=wipe
export SWWW_TRANSITION_STEP=100
export SWWW_TRANSITION_DURATION=10
export SWWW_TRANSITION_FPS=60
export SWWW_TRANSITION_ANGLE=225
export SWWW_TRANSITION_BEZIER=0.66,1,0.60,1

# Directory containing wallpapers
wallpaper_dir="$HOME/.wallpapers/HyprGoat/"

# Initialize if not already done
swww query || swww init

# Use swww query along with grep and awk to get current active wallpaper in the form of "<number>.<ext>"
current_wallpaper=$(swww query | grep -oP 'image: "\K[^"]*')

# Extract the number portion from "<number>.<ext>"
current_wallpaper_number=${current_wallpaper%.*}

# Increment the wallpaper number
next_wallpaper_number=$((current_wallpaper_number + 1))

# Array of permissible file extensions
file_extensions=("jpg" "png" "webp" "gif")

# Look for next available image file
found=0
for ext in "${file_extensions[@]}"; do
    next_wallpaper="${wallpaper_dir}${next_wallpaper_number}.${ext}"
    if [[ -f "$next_wallpaper" ]]; then
        found=1
        break
    fi
done

# If no next wallpaper was found in any file format, reset to the first available wallpaper
if [[ "$found" -eq 0 ]]; then
    for ext in "${file_extensions[@]}"; do
        first_wallpaper="${wallpaper_dir}1.${ext}"
        if [[ -f "$first_wallpaper" ]]; then
            next_wallpaper=$first_wallpaper
            break
        fi
    done
fi

# Set the new wallpaper using the img command and provide the full path
swww img "$next_wallpaper"

