#!/bin/bash

export SWWW_TRANSITION=wipe
export SWWW_TRANSITION_STEP=100
export SWWW_TRANSITION_DURATION=10
export SWWW_TRANSITION_FPS=60
export SWWW_TRANSITION_ANGLE=45
export SWWW_TRANSITION_BEZIER=0.66,1,0.60,1

# Directory containing wallpapers
wallpaper_dir="$HOME/.wallpapers/HyprGoat/"

# Initialize if not already done
swww query || swww init

# Use swww query along with grep to get current active wallpaper in the form of "<number>.<ext>"
current_wallpaper=$(swww query | grep -oP 'image: "\K[^"]*')

# Extract the number portion from "<number>.<ext>"
current_wallpaper_number=$(basename $current_wallpaper | grep -oP '^\d+')

# Decrement the wallpaper number
prev_wallpaper_number=$(($current_wallpaper_number - 1))

# Array of permissible file extensions
file_extensions=("jpg" "png" "webp" "gif")

# Look for prev available image file
found=0
for ext in "${file_extensions[@]}"; do
    prev_wallpaper="${wallpaper_dir}${prev_wallpaper_number}.${ext}"
    if [[ -f "$prev_wallpaper" ]]; then
        found=1
        break
    fi
done

# If we're at the first wallpaper or no previous file found, find the highest numbered wallpaper to wrap around
if [[ "$prev_wallpaper_number" -eq "0" ]] || [[ "$found" -eq "0" ]]; then
    highest_wallpaper_number=0
    highest_wallpaper_ext=""
    # Loop for every extension
    for ext in "${file_extensions[@]}"; do
        # Extract number from filename
        max_wallpaper_number=$(ls -v "${wallpaper_dir}"*."${ext}" 2> /dev/null | grep -oP '\d+(?=\.'${ext}')' | sort -nr | head -n1)
        if [[ -n "$max_wallpaper_number" ]] && [[ "$max_wallpaper_number" -gt "$highest_wallpaper_number" ]]; then
            highest_wallpaper_number=$max_wallpaper_number
            highest_wallpaper_ext=$ext
        fi
    done
    prev_wallpaper_number=$highest_wallpaper_number
    prev_wallpaper="${wallpaper_dir}${prev_wallpaper_number}.${highest_wallpaper_ext}"
fi

# Set the new wallpaper using the img command and provide full path
swww img "$prev_wallpaper"
