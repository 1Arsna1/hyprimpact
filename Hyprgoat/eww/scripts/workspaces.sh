#! /bin/bash

function clamp {
    min=\$1
    max=\$2
    val=\$3
    python -c "print(max($min, min($val, $max)))"
}

function switch_workspace {
    direction=\$1
    current=\$2
    if test "$direction" = "down"
    then
        target=$(clamp 1 10 $(($current+1)))
        echo "jumping to $target"
        hyprctl dispatch workspace $target
    elif test "$direction" = "up"
    then
        target=$(clamp 1 10 $(($current-1)))
        echo "jumping to $target"
        hyprctl dispatch workspace $target
    fi
}

function spaces (){
    ACTIVE_WORKSPACE=$(hyprctl monitors -j | jq '.[] | select(.focused) | .activeWorkspace.id' | sed 's/\"//g')
    WORKSPACE_WINDOWS=$(hyprctl workspaces -j | jq 'map({key: .id | tostring, value: .windows}) | from_entries')
    seq 1 10 | jq --argjson windows "${WORKSPACE_WINDOWS}" --arg active "${ACTIVE_WORKSPACE}" --slurp -Mc 'map({id: ., windows: ($windows[.]//0)}) | .[] | if .id == $active then .id="\uf120 "+.id else .id="\uf10c "+.id end'
}

function get_active_workspace {
    hyprctl monitors -j | jq '.[] | select(.focused) | .activeWorkspace.id'
}

# Combine the above functions
# switch_workspace
# spaces
# get_active_workspace
