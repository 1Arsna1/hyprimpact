#!/bin/bash

# Function to get the current volume percentage
function get_volume {
    # extract the volume level from the 5th line of pactl list sinks output
    pactl list sinks | grep 'Volume: f' | awk '{print $5}'
}

# Function to set the volume
function set_volume {
    # set the sink volume
    pactl set-sink-volume @DEFAULT_SINK@ \$1
}

# Check if a parameter is passed
if [[ ! -z "$1" ]]; then
    set_volume \$1
fi

# Print current volume level
echo $(get_volume)
