#!/bin/bash

export SWWW_TRANSITION=wipe
export SWWW_TRANSITION_STEP=100
export SWWW_TRANSITION_DURATION=10
export SWWW_TRANSITION_FPS=60
export SWWW_TRANSITION_ANGLE=45
export SWWW_TRANSITION_BEZIER=0.66,1,0.60,1

# Directory containing wallpapers
wallpaper_dir="$HOME/.wallpapers/HyprGoat/"

# Initialize if not already done
swww query || swww init

# Use swww query along with grep and awk to get current active wallpaper
current_wallpaper=$(swww query | grep -oP 'currently displaying: image: \K[^"]*')

echo "Current wallpaper: $current_wallpaper"

# Array of permissible file extensions
file_extensions=("jpg" "png" "webp" "gif")

# Create an array to hold all valid wallpapers
wallpapers=()

# Populate the array with valid wallpapers sorted by number
for ext in "${file_extensions[@]}"; do
    while IFS=  read -r -d $'\0'; do
        wallpapers+=("$REPLY")
    done < <(find $wallpaper_dir -name "*.$ext" -print0 | sort -z)
done

echo "All wallpapers: ${wallpapers[@]}"

# Find the index of the current wallpaper in the array
current_index=-1
for index in "${!wallpapers[@]}"; do
    if [[ "${wallpapers[$index]}" == *"$current_wallpaper"* ]]; then
        current_index=$index
        break
    fi
done

echo "Current index: $current_index"

# Get the number of wallpapers
num_wallpapers=${#wallpapers[@]}

echo "Number of wallpapers: $num_wallpapers"

# If the current wallpaper was found in the array, set the next wallpaper to the previous one in the array
# If the current wallpaper is the first one in the array or the previous wallpaper could not be set, set the wallpaper to the last one in the array
if [[ $current_index -ne -1 ]]; then
    if [[ $current_index -gt 0 ]]; then
        previous_wallpaper=${wallpapers[$((current_index-1))]}
        echo "Previous wallpaper: $previous_wallpaper"
        swww img "$previous_wallpaper"
        if [[ $? -ne 0 ]]; then
            echo "Could not set previous wallpaper. Setting last wallpaper."
            swww img "${wallpapers[$((num_wallpapers-1))]}" --filter Nearest --resize fit
        fi
    else
        echo "Current wallpaper is the first one. Setting last wallpaper."
        swww img "${wallpapers[$((num_wallpapers-1))]}" --filter Nearest --resize fit
    fi
fi
