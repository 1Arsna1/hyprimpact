#!/bin/bash

systemctl --user stop pipewire.service wireplumber pipewire.socket
systemctl --user start pipewire.service wireplumber pipewire.socket

sleep 1
killall waybar
waybar &
killall nm-applet
nm-applet --indicator &
killall dunst
dunst &
killall swww
swww init &
killall swayosd
swayosd &
killall polkit-mate-authentication-agent-1
/usr/lib/mate-polkit/polkit-mate-authentication-agent-1 &
killall wl-paste
wl-paste --type text --watch cliphist store &
wl-paste --type image --watch cliphist store &
#uncomment when using laptop
killall blueman
blueman-applet &
